export PATH="$HOME/.local/bin:$PATH"

if [ "$XDG_SESSION_TYPE" = "wayland" ]; then

    export MOZ_ENABLE_WAYLAND=1

fi

if [ -e "$HOME/.cargo/env" ]; then

    . "$HOME/.cargo/env"

fi

if which go &>/dev/null; then
    export GOPATH=$HOME/.local/go
fi
